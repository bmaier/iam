# Gaia-X TC WG Identity & Access Management - Meeting notes for 10/05/2022

## Minutes

* IP issue : [the Architecture Document](https://docs.google.com/document/d/1tpIgEXC-3h-N6Xk5NhoWtdfQuXT_wkVITKzNVSaKRdI/edit#heading=h.f26y2rhalgq8) could have an IP issue : an [issue](https://gitlab.com/gaia-x/technical-committee/federation-services/iam/-/issues/10) on this point has been assigned to Pierre
* The group members should start to update the [Authentication / Authorization](https://gaia-x.gitlab.io/technical-committee/federation-services/federation-service-specifications/L01_IDM_AA/idm_aa/) specifications, keeping in mind this document cover mainly Authentication subject for now.
* The group members is invited to think about **Authorization** (not covered right now) :
  * By privileging a decentralized mode (like for Authentication)
  * That can be seamlessly integrated with the SSI ecosystem (DID / VC)
  * Than is compatible with SSI <-> OpenID bridge built for Authentication

## Authorization discussion

There is some existing decentralized Authorization management technologies on the market :
* https://www.openpolicyagent.org
* https://www.biscuitsec.org

We have to evaluate their possibilities of integration in the SSI world.

Steffen Schulze propose a decentralized mechanism which is based on VC / DID, a draft document (sequence diagram) should be sent to participants this week.

## Technical demo

Kai Meinke propose to organize a short demo (30') of the Gaia-X Lab demonstrator for OIDC4VP/SIOP flows for onboarding, accreditation and access management during the next meeting (17-05-2022)